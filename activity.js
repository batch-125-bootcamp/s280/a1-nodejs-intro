// 1. What directive is used by Node.js in loading the modules it needs?

	ANSWER: require();

// 2. What node.js module contains a method for server creation?

	ANSWER: http

// 3. What is the method of the http object responsible for creating a server using node.js?
	
	ANSWER: createServer();

// 4. What method of the response object allows us to set status codes and content types?

	ANSWER: writeHead({});

// 5. Where will console.log() output its contents when run in node.js?

	ANSWER: back-end

// 6. What property of the request object contains the address's endpoint?

	ANSWER: url