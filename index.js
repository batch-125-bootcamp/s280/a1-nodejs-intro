let http = require("http");
 const port = 3000;

 http.createServer((request, response) => {

 	if (request.url == "/login"){
 		response.writeHead(200, {"Content-Type" : "text/html"});
 		response.end("Congratulations! You are in the login page.");
 	} else {
 		response.writeHead(404, {"Content-Type" : "text/html"});
 		response.end("I am sorry but page not found.");
 	}

 }).listen(port);

 console.log("Server is successfully running!")